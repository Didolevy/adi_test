export interface Student {
    name: string;
    avmat: number;
    psy: number;
    pay:string;
    id?:string;
    saved?:Boolean;
    result?:string; 
}
