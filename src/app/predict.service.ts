import { Student } from './interfaces/student';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url = " https://9b6phrt9sh.execute-api.us-east-1.amazonaws.com/beta";
  studentCollection:AngularFirestoreCollection; 
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  

  predict(psy:number,avmat:number,pay:string){
    let json = {
      "psy": psy,
      "avmat": avmat,
      "pay":pay
    }
    let body = JSON.stringify(json)
    console.log(body);
    return this.http.post<any>(this.url ,body).pipe(
    map(res =>{
      console.log(res);
      let final:string = res.body;  
      console.log(final);
      final = final.replace('[',''); 
      final = final.replace(']','');
      return final; 
    })
    )
  }


  constructor(private http:HttpClient, private db:AngularFirestore) { }
}
