import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  email:string; 
  user$;

  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.user$ = this.authService.getUser(); 
    this.user$.subscribe(
      data => {
        this.email= data.email;
  })

  }
}
