import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email:string;
  password:string; 
  errorMessage:string; 
  isError:boolean = false; 
  

onSubmit(){
  this.authService.signup(this.email,this.password).then(
    res=>{
      this.router.navigate(['/welcome']);}
  ).catch(
    err=>{
      this.isError=true;
      this.errorMessage=err.message;

    }
  )
    }




    constructor(private authService:AuthService, private router:Router) { }

   
  

  ngOnInit(): void {
  }

}
