import { Student } from './interfaces/student';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  studentCollection:AngularFirestoreCollection; 
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  
  addstudent(userId:string,pay:string,avmat:number,result:string,psy:number){
    const student = {psy:psy,result:result,avmat:avmat,pay:pay}; 
    this.userCollection.doc(userId).collection('students').add(student);
  }

  public getstudent(userId){
    this.studentCollection = this.db.collection(`users/${userId}/students`); 
    return this.studentCollection.snapshotChanges()
    
  } 
  public deletestudent(userId:string , id:string){
    this.db.doc(`users/${userId}/students/${id}`).delete();
  }

  constructor(private db:AngularFirestore) { }
}
