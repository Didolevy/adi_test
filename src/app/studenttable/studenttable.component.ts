import { Student } from './../interfaces/student';
import { AuthService } from './../auth.service';
import { StudentService } from './../student.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-studenttable',
  templateUrl: './studenttable.component.html',
  styleUrls: ['./studenttable.component.css']
})
export class StudenttableComponent implements OnInit {
  student$; 
  students: Student[];
  userId:string;

  panelOpenState = false;
  constructor(private StudentService:StudentService,private auth: AuthService) {
  }

  ngOnInit(): void {
    this.auth.user.subscribe(res => {
      this.userId = res.uid
      this.student$ = this.StudentService.getstudent(this.userId);
      this.student$.subscribe(docs => {
        this.students = [];
        for(let document of docs) {
          const student: Student = document.payload.doc.data();
          student.id = document.payload.doc.id;
          this.students.push(student);
        };
        console.log(this.students);
      })
    });

  }
  deletestudent(id:string){
    this.StudentService.deletestudent(this.userId,id);
  }
}
