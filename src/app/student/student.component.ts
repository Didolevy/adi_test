import { AuthService } from './../auth.service';
import { StudentService } from './../student.service';
import { PredictService } from './../predict.service';
import { Student } from './../interfaces/student';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  student:Student[];
  checked = false;
  paies:Object[] = [{id:1,name:'true'},{id:2,name:'false'}]
  pay:string;
  haveError: boolean = false;
  @Input () avmat:number;
  @Input () psy:number;
  result:string;
  userId:string;
  
  
  
  constructor(private predictService:PredictService, private studentService:StudentService,private auth: AuthService) {
    auth.user.subscribe(res => this.userId = res.uid);
   }
   add(){
    this.studentService.addstudent(this.userId,this.pay,this.avmat,this.result,this.psy); 
  }


  ngOnInit(): void {
  }
  predict(){
    console.log(this.psy,this.avmat,this.pay)
    this.predictService.predict(this.psy,this.avmat,this.pay).subscribe(
      res => {
        console.log(res);
        let adi = Number(res);
        if(adi > 0.5) {
          this.result = 'Not Drop';
        } else {
          this.result = 'Drop';
        }

      }
    )

     
    }
  
    
    }
