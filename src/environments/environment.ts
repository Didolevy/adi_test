// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAuNEVRP5q6vFzGWQ88b2e0k_8kzNXCmLI",
    authDomain: "aditest-8fb11.firebaseapp.com",
    projectId: "aditest-8fb11",
    storageBucket: "aditest-8fb11.appspot.com",
    messagingSenderId: "363509351657",
    appId: "1:363509351657:web:b4562ee61ee7e904695c78"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
